.. Unicode для программистов documentation master file, created by
   sphinx-quickstart on Mon Jan 18 09:48:04 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unicode для программистов!
==========================

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   unicode/unicode-intro
   unicode/unicode-abbrev
   unicode/unicode-glossary


Индекс и указатели
==================

* :ref:`genindex`
