


Glossary
--------

.. glossary::

   code point
       код -- числовое значение символа Unicode

   code unit
       кодируемое слово

   low-surrogate code unit
       младший суррогат

   high-surrogate code unit
       старший суррогат

   surrogate pair
       суррогатная пара
